var mysql = require("mysql");

const PersonDao = require("./persondao.js");
const runsqlfile = require("./runsqlfile.js");
var updated;
// GitLab CI Pool
var pool = mysql.createPool({
  connectionLimit: 2, //Må ha poolsize 2 siden jeg har et indre callback inne i en yttre callback linje: 103 og 104
  host: "mysql",
  user: "root",
  password: "secret",
  database: "supertestdb",
  debug: false,
  multipleStatements: true
});

let personDao = new PersonDao(pool);

beforeAll(done => {
  runsqlfile("dao/create_tables.sql", pool, () => {
    runsqlfile("dao/create_testdata.sql", pool, done);
  });
});

afterAll(() => {
  pool.end();
});

test("get one person from db", done => {
  function callback(status, data) {
    console.log(
      "Test callback: status=" + status + ", data=" + JSON.stringify(data)
    );
    expect(data.length).toBe(1);
    expect(data[0].navn).toBe("Hei Sveisen");
    done();
  }

  personDao.getOne(1, callback);
});

test("get unknown person from db", done => {
  function callback(status, data) {
    console.log(
      "Test callback: status=" + status + ", data=" + JSON.stringify(data)
    );
    expect(data.length).toBe(0);
    done();
  }

  personDao.getOne(0, callback);
});

test("add person to db", done => {
  function callback(status, data) {
    console.log(
      "Test callback: status=" + status + ", data=" + JSON.stringify(data)
    );
    expect(data.affectedRows).toBeGreaterThanOrEqual(1);
    done();
  }

  personDao.createOne(
    { navn: "Nils Nilsen", alder: 34, adresse: "Gata 3" },
    callback
  );
});

test("get all persons from db", done => {
  function callback(status, data) {
    console.log(
      "Test callback: status=" + status + ", data.length=" + data.length
    );
    expect(data.length).toBeGreaterThanOrEqual(2);
    done();
  }

  personDao.getAll(callback);
});

/* OPPGAVE 3 del 1 */
test("delete one person from db", done => {
  var initial_length;

  function callback(status, data){
    console.log(
      "Test callback: status=" + status + ", data=" + JSON.stringify(data)
    );
    expect(data.affectedRows).toBe(1);
    done();
  }

  function lengthCallback(status, data){
    initial_length = data.length;
    personDao.deleteOne(1, callback);
  }

  personDao.getAll(lengthCallback);
});

/* OPPGAVE 3 del 2 */

test("Update person name to db", done => {
  function subcallback(status, data2){
      console.log(
        "Test subcallback: status=" + status + ", data=" + JSON.stringify(data2)
      );
      expect(data2[0].navn).toBe("Frank Kjosas");
      done();
  } 

  function callback(status, data){
    console.log(
        "Test subcallback: status=" + status + ", data=" + JSON.stringify(data)
    );
    personDao.getOne(2, subcallback);
    
  }
  personDao.updateOne(2, {navn: "Frank Kjosas", alder: 52, adresse: "Kongensgate 1"}, callback);
});













